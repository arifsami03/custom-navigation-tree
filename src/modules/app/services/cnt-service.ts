import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

import {CNTModel} from 'modules/app/models/cnt-model';

@Injectable()
export class CNTService {

    private dataUrl: String = './assets/data/custom-tree.data.json';

    constructor(private http: Http) {
    }

    getData(): Observable<CNTModel[]> {
        return this.http.get(`${this.dataUrl}`).pipe(map(data => {
            return <CNTModel[]>data.json();
        }));
    }

}

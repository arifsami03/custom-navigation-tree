// Import components and services etc here
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {HttpModule} from '@angular/http';

import { MatCardModule, MatButtonModule, MatIconModule, MatTreeModule } from '@angular/material';

import { CNTService } from 'modules/app/services';

import { AppComponent, CustomNavigationTreeComponent, TreeNestedComponent } from './components';

export const __IMPORTS = [
    BrowserAnimationsModule,
    HttpModule,
    MatButtonModule,
    MatCardModule,
    MatIconModule,
    MatTreeModule
];

export const __DECLARATIONS = [AppComponent, CustomNavigationTreeComponent, TreeNestedComponent];

export const __PROVIDERS = [CNTService];

export const __ENTRY_COMPONENTS = [];

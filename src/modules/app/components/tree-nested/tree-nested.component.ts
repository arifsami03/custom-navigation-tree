import { Component } from '@angular/core';
import { NestedTreeControl } from '@angular/cdk/tree';
import { MatTreeNestedDataSource } from '@angular/material/tree';

import { FileDatabase, FileNode } from './../../services/file-database.service';
/**
 * @title Tree with nested nodes
 */
@Component({
  selector: 'tree-nested',
  templateUrl: './tree-nested.component.html',
  styleUrls: ['./tree-nested.component.css'],
  providers: [FileDatabase]
})
export class TreeNestedComponent {
  nestedTreeControl: NestedTreeControl<FileNode>;
  nestedDataSource: MatTreeNestedDataSource<FileNode>;

  constructor(database: FileDatabase) {
    this.nestedTreeControl = new NestedTreeControl<FileNode>(this._getChildren);
    this.nestedDataSource = new MatTreeNestedDataSource();

    database.dataChange.subscribe(data => this.nestedDataSource.data = data);

    console.log('tree-nested dataSource: ', this.nestedDataSource.data);
  }

  hasNestedChild = (_: number, nodeData: FileNode) => !nodeData.type;

  private _getChildren = (node: FileNode) => node.children;
}
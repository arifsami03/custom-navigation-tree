import { Component, OnInit } from '@angular/core';
import { NestedTreeControl } from '@angular/cdk/tree';
import { MatTreeNestedDataSource } from '@angular/material/tree';

import { CNTService } from 'modules/app/services';
import { CNTModel } from 'modules/app/models';

@Component({
  selector: 'custom-navigation-tree',
  templateUrl: './custom-navigation-tree.component.html',
  styleUrls: ['./custom-navigation-tree.component.css'],
})
export class CustomNavigationTreeComponent implements OnInit {

  public loaded = false;

  public nestedTreeControl: NestedTreeControl<CNTModel>;
  public nestedDataSource: MatTreeNestedDataSource<CNTModel>;

  constructor(private cntService: CNTService) {

    this.nestedTreeControl = new NestedTreeControl<CNTModel>(this._getChildren);
    this.nestedDataSource = new MatTreeNestedDataSource();

  }

  ngOnInit() {

    this.cntService.getData().subscribe(response => {

      this.nestedDataSource.data = response;
      this.loaded = true;

    }, error => {

      console.log('error: ', error);
      
    }, () => { }
    );

  }

  public hasNestedChild = (_: number, nodeData: CNTModel) => !nodeData.selectable;

  private _getChildren = (node: CNTModel) => node.children;

  public checkBoxChanged(node, event) {
    if (event.target.checked) {

      console.log(node.label + ' is checked.');

    } else {

      console.log(node.label + ' is unchecked.');

    }

  }

}

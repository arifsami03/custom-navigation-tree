import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './components';

import {
  __IMPORTS,
  __DECLARATIONS,
  __PROVIDERS
} from './components.barrel';

@NgModule({
  declarations: [
    __DECLARATIONS
  ],
  imports: [
    BrowserModule,
    __IMPORTS
  ],
  providers: [__PROVIDERS],
  bootstrap: [AppComponent]
})
export class AppModule { }

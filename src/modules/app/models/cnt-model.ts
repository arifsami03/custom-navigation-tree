export class CNTModel {

    public id: number;
    public label: string;
    public selectable: boolean;
    public children?: CNTModel[];

    constructor(){}
}